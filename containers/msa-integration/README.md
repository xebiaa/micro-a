# Micro services project containers

Pre-requisites
--------------
- Vagrant
- Docker
- Boot2Docker

Make sure that virtualbox is your default Vagrant provider (in case you also use VMWare or something else).

> export VAGRANT_DEFAULT_PROVIDER=virtualbox


Start up containers
-------------------

In folder: integration

> vagrant up --provider=docker

Shut down everything
--------------------

In folder: 
1. integration
2. docker-host

> vagrant destroy -f