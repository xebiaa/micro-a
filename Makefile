all: domain.png domain-minimal.png

%.png: %.gv
	dot -Tpng $< > $@
