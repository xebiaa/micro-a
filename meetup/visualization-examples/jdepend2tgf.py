#!/usr/bin/python

import xml.dom.minidom as dom
import sys

jdependDOM = dom.parse(sys.stdin)
print "#"
for package in jdependDOM.childNodes.item(0).childNodes.item(1).childNodes: 
	if package.nodeType==dom.Node.ELEMENT_NODE and package.nodeName=="Package":
		name = package.getAttribute("name").rstrip()
		for deps in package.getElementsByTagName("DependsUpon"):
			for dep in deps.childNodes:
				if dep.nodeType==dom.Node.ELEMENT_NODE:
					print "%s\t%s" % (name, dep.firstChild.nodeValue.rstrip())
