package com.xebia.msa.inventory.feed;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.xebia.msa.inventory.domain.Product;
import com.xebia.msa.inventory.repositories.ProductMemoryRepository;
import com.rometools.rome.feed.synd.*;
import com.rometools.rome.io.SyndFeedOutput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.io.StringWriter;
import java.io.Writer;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by marco
 */

@RestController
@RequestMapping(value="/inventory/products/feed")
public class ProductRSSFeed {

    private static Logger log = LoggerFactory.getLogger(ProductRSSFeed.class);

    @Autowired
    private ProductMemoryRepository productRepository;

    @RequestMapping(method=RequestMethod.GET, produces="application/rss+xml")
    public @ResponseBody String getFeed(@RequestBody String body, @RequestHeader HttpHeaders headers, HttpServletRequest httpServletRequest) {

        String result = "";
        try {
            SyndFeed feed = new SyndFeedImpl();
            feed.setFeedType("rss_2.0");
            feed.setTitle("inventory feed");

            URL serverURL = new URL(httpServletRequest.getScheme(),
                    httpServletRequest.getServerName(),
                    httpServletRequest.getServerPort(),
                    "");

            feed.setLink(serverURL + httpServletRequest.getRequestURI().toString());
            feed.setDescription("This feed provides a list of items that are no longer in stock.");

            List entries = new ArrayList();
            for(Product product: productRepository.findAll()) {
                entries.add(getProductAsSyndEntry(product, serverURL + httpServletRequest.getRequestURI().toString()));
            }
            feed.setEntries(entries);
            Writer writer = new StringWriter();
            SyndFeedOutput output = new SyndFeedOutput();
            output.output(feed, writer);
            result = writer.toString();
            writer.close();
        } catch (Exception ex) {
            log.error(ex.getStackTrace().toString());
            System.out.println("ERROR: " + ex.getMessage());
        }
        return result;
    }

    private SyndEntry getProductAsSyndEntry(Product product, String url) throws Exception {
        SyndEntry entry = new SyndEntryImpl();
        entry.setLink(url + "/" + product.getIdentifier());
        entry.setPublishedDate(new Date());

        SyndContent description = new SyndContentImpl();
        description.setType("application/json");
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd MMM yyyy");
        mapper.setDateFormat(outputFormat);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        description.setValue(mapper.writeValueAsString(product));
        entry.setDescription(description);

        return entry;
    }
}
