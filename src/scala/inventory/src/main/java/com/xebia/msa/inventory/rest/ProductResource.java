package com.xebia.msa.inventory.rest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import javax.validation.constraints.NotNull;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */

public class ProductResource extends ResourceSupport {

    private UUID identifier;
    private String description;
    private Integer inStock;
    private String productCode;
    private String make;

    @JsonCreator
    public ProductResource(@JsonProperty(value = "identifier") @NotNull UUID identifier,
                           @JsonProperty(value = "description") @NotNull String description,
                           @JsonProperty(value = "inStock") @NotNull Integer inStock,
                           @JsonProperty(value = "productCode") @NotNull String productCode,
                           @JsonProperty(value = "make") @NotNull String make) {

        this.identifier = identifier;
        this.description = description;
        this.inStock = inStock;
        this.productCode = productCode;
        this.make = make;
    }

    public ProductResource() {
    }

    public UUID getIdentifier() {
        return identifier;
    }

    public String getDescription() {
        return description;
    }

    public Integer getInStock() {
        return inStock;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getMake() {
        return make;
    }

    public void setIdentifier(UUID identifier) {
        this.identifier = identifier;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public void setMake(String make) {
        this.make = make;
    }
}
