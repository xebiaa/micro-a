package com.xebia.msa.inventory.rest;


import com.xebia.msa.inventory.domain.Product;
import org.springframework.beans.BeanUtils;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

/**
 * Created by marco on 06/11/14.
 */
public class ProductResourceAssembler extends ResourceAssemblerSupport<Product, ProductResource> {

    public ProductResourceAssembler() {
        super(ProductController.class, ProductResource.class);
    }

    @Override
    public ProductResource toResource(Product product) {

        ProductResource resource = createResourceWithId(product.getIdentifier(), product);
        BeanUtils.copyProperties(product, resource);

        return resource;
    }

    public Product toClass(ProductResource resource){

        return new Product(resource.getIdentifier(), resource.getDescription(), resource.getInStock(), resource.getProductCode(), resource.getMake());

    }
}
