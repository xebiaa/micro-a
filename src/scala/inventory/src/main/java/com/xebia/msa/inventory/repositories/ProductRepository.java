package com.xebia.msa.inventory.repositories;

import com.xebia.msa.inventory.domain.Product;

import java.util.List;
import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */


public interface ProductRepository {

    Product save(Product order);

    boolean delete(UUID key);

    Product findById(UUID key);

    List<Product> findAll();
}

