package com.xebia.msa.inventory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.xebia.msa.inventory.repositories.ProductMemoryRepository;
import com.xebia.msa.inventory.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.*;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageProducer;
import org.springframework.integration.kafka.core.ConnectionFactory;
import org.springframework.integration.kafka.core.DefaultConnectionFactory;
import org.springframework.integration.kafka.core.ZookeeperConfiguration;
import org.springframework.integration.kafka.inbound.KafkaMessageDrivenChannelAdapter;
import org.springframework.integration.kafka.listener.KafkaMessageListenerContainer;
import org.springframework.integration.kafka.support.ZookeeperConnect;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.GenericMessage;

import java.util.Arrays;

/**
 * Created by marco on 03/11/14.
 */

@Configuration
@EnableAutoConfiguration
@ComponentScan
@EnableIntegration
@ImportResource("/xml/outbound-kafka-integration.xml")
public class Application {

    private Log log = LogFactory.getLog(getClass());


 /*   @Bean
    @DependsOn("kafkaOutboundChannelAdapter")
    CommandLineRunner kickOff(@Qualifier("inputToKafka") MessageChannel in) {
        return args -> {
            for (int i = 0; i < 1000; i++) {
                in.send(new GenericMessage<>("#" + i));
                log.info("sending message #" + i);
            }
        };
    }
  */


    @Bean
    public ProductRepository createRepository() {
        return new ProductMemoryRepository();
    }


    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(Application.class, args);


       /* System.out.println("Let's inspect the beans provided by Spring Boot:");

        String[] beanNames = applicationContext.getBeanDefinitionNames();
        Arrays.sort(beanNames);
        for (String beanName : beanNames) {
            System.out.println(beanName);
        }
       */
    }
}
