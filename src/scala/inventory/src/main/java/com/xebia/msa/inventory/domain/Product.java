package com.xebia.msa.inventory.domain;

import java.util.UUID;

/**
 * Created by marco on 03/11/14.
 */
public class Product {

    private UUID identifier;
    private String description;
    private Integer inStock;
    private String productCode;
    private String make;

    public Product(UUID identifier, String description, Integer inStock, String productCode, String make) {
        this.identifier = identifier;
        this.description = description;
        this.inStock = inStock;
        this.productCode = productCode;
        this.make = make;
    }

    public UUID getIdentifier() {
        return identifier;
    }

    public String getDescription() {
        return description;
    }

    public Integer getInStock() {
        return inStock;
    }

    public String getProductCode() {
        return productCode;
    }

    public String getMake() {
        return make;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setInStock(Integer inStock) {
        this.inStock = inStock;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return productCode.equals(product.productCode);

    }

    @Override
    public int hashCode() {
        return productCode.hashCode();
    }
}

