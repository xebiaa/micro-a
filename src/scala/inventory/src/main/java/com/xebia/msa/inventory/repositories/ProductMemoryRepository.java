package com.xebia.msa.inventory.repositories;


import com.xebia.msa.inventory.domain.Product;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by marco on 03/11/14.
 */
@Component
public class ProductMemoryRepository implements ProductRepository {


    private Map<UUID, Product> products = Collections.unmodifiableMap(new HashMap<UUID, Product>());

    //public OrderMemoryRepository(final Map<UUID, Order> orders) {
    //    this.orders = Collections.unmodifiableMap(orders);
    //}

    public ProductMemoryRepository(){
        Map<UUID, Product> modifiableProducts = new HashMap<UUID, Product>(products);

        for (int i = 0; i < 100; i++) {
            Random random = new Random();

            Product product = new Product(UUID.randomUUID(), "ProductItem_"+i, random.nextInt(100), "ASF76ST54"+random.nextInt(100), "Article Description "+i);
            modifiableProducts.put(product.getIdentifier(), product);
        }

        this.products = Collections.unmodifiableMap(modifiableProducts);
    }

    @Override
    public synchronized Product save(Product product) {

        Map<UUID, Product> modifiableProducts = new HashMap<UUID, Product>(products);
        modifiableProducts.put(product.getIdentifier(), product);
        this.products = Collections.unmodifiableMap(modifiableProducts);

        return product;
    }

    @Override
    public synchronized boolean delete(UUID key) {
        boolean deleted = false;
        if (products.containsKey(key)) {
            Map<UUID, Product> modifiableProducts = new HashMap<UUID, Product>(products);
            modifiableProducts.remove(key);
            this.products = Collections.unmodifiableMap(modifiableProducts);
            deleted = true;
        }
        return deleted;
    }

    @Override
    public Product findById(UUID key) {
        return products.get(key);
    }

    @Override
    public List<Product> findAll() {
        return Collections.unmodifiableList(new ArrayList<Product>(products.values()));
    }

}
