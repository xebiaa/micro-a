package com.xebia.msa.inventory.rest;

import com.xebia.msa.inventory.domain.Product;
import com.xebia.msa.inventory.repositories.ProductMemoryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by marco on 03/11/14.
 */
@RestController
@RequestMapping("/inventory/products")

public class ProductController {

    private static Logger LOG = LoggerFactory.getLogger(ProductController.class);


    @Autowired
    private MessageChannel inputToKafka;

    @Autowired
    private ProductMemoryRepository productRepository;

    private ProductResourceAssembler assembler = new ProductResourceAssembler();


    @RequestMapping(method = RequestMethod.GET)
    public List<ProductResource> allProducts() {
        List<ProductResource> orderResources = new ArrayList<ProductResource>();

        orderResources = assembler.toResources(productRepository.findAll());

        return orderResources;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    public ResponseEntity<ProductResource> viewProduct(@PathVariable String id) {

        Product product = productRepository.findById(UUID.fromString(id));

        if (product == null) {
            return new ResponseEntity<ProductResource>(HttpStatus.NOT_FOUND);
        }
        ProductResource resource = assembler.toResource(product);

        return new ResponseEntity<ProductResource>(resource, HttpStatus.OK);
    }

    /**
     * POST new Product
     */
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/hal+json")
    public ResponseEntity<ProductResource> createNewProductInInventory(@RequestBody NewProductResource newProductResource) {

        Product product = new Product(UUID.randomUUID(), newProductResource.getDescription(), newProductResource.getInStock(), newProductResource.getProductCode(), newProductResource.getMake());

        Product responseProduct = productRepository.save(product);
        // signal event to kafka


        inputToKafka.send(
                MessageBuilder.withPayload(product.getDescription())
                        //.setHeader(KafkaHeaders.MESSAGE_KEY, product.getIdentifier())  // Note: the header was `messageKey` in earlier versions
                        //.setHeader(KafkaHeaders.TOPIC, "test")       // Note: the header was `topic` in earlier versions
                        .build()
        );

        ProductResource responseResource = new ProductResourceAssembler().toResource(responseProduct);
        HttpHeaders headers = new HttpHeaders();
        headers.add("ETag", Integer.toString(responseProduct.hashCode()));
        headers.setLocation(linkTo(methodOn(getClass()).viewProduct(responseProduct.getIdentifier().toString())).toUri());
        return new ResponseEntity<ProductResource>(responseResource, headers, HttpStatus.CREATED);
    }


}
