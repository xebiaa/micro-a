package com.xebia.shipping.domain

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class DeliveryData (date:Int)

object DeliveryData extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val deliveryFormat = jsonFormat1(DeliveryData.apply)
}

