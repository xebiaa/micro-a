package com.xebia.shipping.service

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import spray.can.Http

import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory

object Boot extends App {
  println("Boot in main")
  def configFactory = ConfigFactory.load
  def defaultDate = configFactory.getString("serviceConf.defaultDate")
  def host = configFactory.getString("serviceConf.HOST")

  def port = configFactory.getInt("serviceConf.PORT")

  implicit val system = ActorSystem("shipping")
  val service = system.actorOf(Props[ShippingServiceActor], "shipping-service")

  def kafkaProducer:KafkaProducer = {
    println("kafkaProducer in main.Boot")
    new KafkaProducer("test", "localhost:9092")
  }

  implicit val timeout = Timeout(5.seconds)
  IO(Http) ? Http.Bind(service, interface = host, port = port)
}
