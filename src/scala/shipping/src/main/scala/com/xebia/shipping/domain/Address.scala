package com.xebia.shipping.domain

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Address(street: String, city: String, uuid:String, modifiedAt:String, orderUuid: String)

object Address extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val addressFormat = jsonFormat5(Address.apply)
}
