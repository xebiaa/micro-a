package com.xebia.shipping.domain

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Xx(x1: Int, x2: Int)

object Xx extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val xxFormat = jsonFormat2(Xx.apply)
}

case class XFromService(x1: Int, x2: Option[Int])

object XFromService extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val xFormat = jsonFormat2(XFromService.apply)

  // TODO: fails if x2 contains emtpy string: 'The request content was malformed:
  //  Expected Int as JsNumber, but got ""'
  def toXx(xFromService: XFromService) =
    Xx(xFromService.x1, xFromService.x2.getOrElse(12345))

}
