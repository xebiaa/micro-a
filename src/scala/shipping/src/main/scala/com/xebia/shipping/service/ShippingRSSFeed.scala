package com.xebia.shipping.service

import java.util.Date

import com.xebia.shipping.domain.Address

import scala.collection.mutable.Map

class ShippingRSSFeed(addresses: Map[String, Address], baseUri: String, modifiedSinceHeader:String) {
  private val modifiedSince = ShippingService.dateFromString(modifiedSinceHeader)
  private val now = ShippingService.dateAsString(new Date)
  val feed =
    <rss version="2.0">
      <channel>
        <title>Shipping Address</title>
        <description>Shipping address</description>
        <link>{baseUri}/rss</link>
        <lastBuildDate>{now}</lastBuildDate>
        <pubDate>{now}</pubDate>
        
        {for (address <- addresses.values if (ShippingService.dateFromString(address.modifiedAt).after(modifiedSince))) yield {
        <item>
          <title>new addres entered at: {address.modifiedAt}</title>
          <description>Ship order to: {address.street} in {address.city}</description>
          <address>
            <street>{address.street}</street>
            <city>{address.city}</city>
            <modifiedAt>{address.modifiedAt}</modifiedAt>
            <uuid>{address.uuid}</uuid>
          </address>
          <link>http://{baseUri}/address/{address.uuid}</link>
          <guid>http://{baseUri}/address/{address.uuid}</guid>
          <pubDate>{now}</pubDate>
        </item>
      }}
        
      </channel>
    </rss>
}
