package com.xebia.shipping.domain

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class TransportCost (uuidForAddress:String, cost:Double)

object TransportCost extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val transportCostFormat = jsonFormat2(TransportCost.apply)
}
