package com.xebia.shipping.domain

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Order (uuid:String, description:String)

object Order extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val orderFormat = jsonFormat2(Order.apply)
}
