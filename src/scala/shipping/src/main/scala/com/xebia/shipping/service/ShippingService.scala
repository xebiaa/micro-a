package com.xebia.shipping.service

import java.text.{DateFormat, SimpleDateFormat}
import java.util.{Date, UUID}

import akka.actor.Actor
import com.xebia.shipping.domain.Order._
import com.xebia.shipping.domain._
import spray.http.HttpRequest
import spray.http.MediaTypes._
import spray.http.StatusCodes._
import spray.httpx.SprayJsonSupport
import spray.json._
import spray.routing._
import spray.routing.directives.{DebuggingDirectives, LoggingMagnet}

import scala.collection.mutable.Map

class ShippingServiceActor extends Actor with ShippingService {
  def actorRefFactory = context

  def receive = runRoute(route)
}

case class AddressFromService(street: String, city: String, uuid: Option[String], modifiedAt: Option[String], orderUuid: String)

object AddressFromService extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val addressFormat = jsonFormat5(AddressFromService.apply)

  implicit def addressListFormat[T: JsonFormat] = jsonFormat1(List.apply[T])

  def toAddress(addressFromService: AddressFromService) =
    Address(addressFromService.street, addressFromService.city, getUuid(addressFromService.uuid), getModifiedAt(addressFromService.modifiedAt), addressFromService.orderUuid)

  def getUuid(uuid: Option[String]): String = {
    if (uuid == None) UUID.randomUUID().toString
    else
      uuid.get match {
        case "" => UUID.randomUUID().toString
        case _ => uuid.get
      }
  }

  def getModifiedAt(date: Option[String]): String = {
    if (date == None) ShippingService.dateAsString(new Date)
    else
      date.get match {
        case "" => ShippingService.dateAsString(new Date)
        case _ => date.get
      }
  }
}

object ShippingService {
  private val dateFormat: DateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z")

  def dateAsString(date: Date) = dateFormat.format(date)

  def dateFromString(date: String) = dateFormat.parse(date)
}

trait ShippingService extends HttpService {
  def getItemsModifiedSince(timeStamp: Long): List[AddressFromService] = List()

  val addresses: Map[String, Address] = Map("uuid1" -> Address("street", "city", "uuid1", ShippingService.dateAsString(new Date), "uuidOrder1"), "uuid2" -> Address("street2", "city2", "uuid2", ShippingService.dateAsString(new Date), "uuidOrder2"))
  val transportCosts: Map[String, TransportCost] = Map()
  val orders: Map[String, Order] = Map("uuidOrder1" -> Order("uuidOrder1", "address for order1")
    , "uuidOrder2" -> Order("uuidOrder2", "address for order2"))

  val route =
    path("rss") {
      pathEnd {
        respondWithMediaType(`application/rss+xml`) {
          optionalHeaderValueByName("If-Modified-Since") {
            case Some(modifiedSince) => complete(new ShippingRSSFeed(addresses, Boot.host + ":" + Boot.port, modifiedSince).feed)
            case _ => complete(new ShippingRSSFeed(addresses, Boot.host + ":" + Boot.port, Boot.defaultDate).feed)
          }
        }
      }
    } ~
      post {
        path("address") {
          respondWithMediaType(`application/json`) {
            entity(as[AddressFromService]) { address =>
              val newAddress: Address = AddressFromService.toAddress(address)
              addresses += (newAddress.uuid -> newAddress)
              sendOrderIfComplete(newAddress)
              complete(Created, newAddress)
            }
          }
        }
      } ~
      post {
        path("xx") {
          respondWithMediaType(`application/json`) {
            entity(as[XFromService]) { xFromService =>
              val newXx= XFromService.toXx(xFromService)
              complete(Created, newXx)
            }
          }
        }
      } ~
      put {
        path("address") {
          respondWithMediaType(`application/json`) {
            entity(as[AddressFromService]) { address =>
              val newAddress: Address = AddressFromService.toAddress(address)
              addresses += (newAddress.uuid -> newAddress)
              sendOrderIfComplete(newAddress)
              complete(OK, newAddress)
            }
          }
        }
      } ~
      path("addresses") {
        complete {
          addresses.values
        }
      } ~
      path("address" / Segment) { uuid =>
        val result = addresses.get(uuid)
        result match {
          case Some(_) => complete(result.get)
          case _ => complete(NotFound, "Address with key '" + uuid + "' not found")
        }
      } ~
      path("transportCost" / Segment) { uuidForAddress =>
        val result = transportCosts.get(uuidForAddress)
        result match {
          case Some(_) => complete(result.get)
          case _ => complete(NotFound, "Transport data for address with key '" + uuidForAddress + "' not found")
        }
      } ~
      post {
        path("transportCost") {
          respondWithMediaType(`application/json`) {
            entity(as[TransportCost]) { transportCost =>
              transportCosts += (transportCost.uuidForAddress -> transportCost)
              complete(Created, transportCost)
            }
          }
        }
      } ~
      put {
        path("transportCost") {
          respondWithMediaType(`application/json`) {
            entity(as[TransportCost]) { transportCost =>
              transportCosts += (transportCost.uuidForAddress -> transportCost)
              complete(OK, transportCost)
            }
          }
        }
      } ~
      path("deliveryTime" / Segment) { uuid =>
          val order = orders.get(uuid)
          order match {
            case Some(_) => complete(calculateDeliveryTime(order.get))
            case _ => complete(NotFound, "Address with key '" + uuid + "' not found")
        }
      } ~
      path("orders") {
        complete {
          orders.values
        }
      } ~
      post {
        path("orderPlaced") {
          respondWithMediaType(`application/json`) {
            entity(as[Order]) { order =>
              orders += (order.uuid -> order)
              sendOrderIfComplete(order)
              complete(Created, order)
            }
          }
        }
      }

  // TODO: clean up
  def sendOrderIfComplete(order:Order):Unit = {
    val addressForOrder = addresses.values.filter (a => a.orderUuid == order.uuid)
    if (addressForOrder.size > 0) {
      val theOrder = orders.get(addressForOrder.head.orderUuid)
      theOrder match {
        case Some(_) => sendOrder(theOrder.get)
        case _ => println("no address yet")
      }
    } else {
      println("no address yet")
    }
  }

  // TODO: clean up
  def sendOrderIfComplete(address:Address):Unit = {
    val order = orders.get(address.orderUuid)
    order match {
      case Some(_) => sendOrder(order.get)
      case _ => println("no order yet")
    }
  }

  def sendOrder(order:Order):Unit = {
    println("Sending order " + order.uuid)
  }

  //TODO: Dummy delivery time
  def calculateDeliveryTime(order: Order): DeliveryData = DeliveryData(2)

  //  lazy val kafkaProducer:KafkaProducer = Boot.kafkaProducer
  //  def kafkaProducer:KafkaProducer = {
  //    println("kafkaProducer in main.Boot")
  //    new KafkaProducer("test", "localhost:9092")
  //  }
  //              kafkaProducer.send("hello world")

  def printResponseStatus(res: Any): Unit = println(res)

  val logResponsePrintln = DebuggingDirectives.logResponse(LoggingMagnet(printResponseStatus))

  def printRequest(req: HttpRequest): Unit = {
    println(req)
  }

  val logRequestPrintln = DebuggingDirectives.logRequest(LoggingMagnet(printRequest))
}
