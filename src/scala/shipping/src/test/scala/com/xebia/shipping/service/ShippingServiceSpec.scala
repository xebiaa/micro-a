package com.xebia.shipping.service

import com.xebia.shipping.domain._
import org.specs2.mutable.Specification
import spray.http.DateTime
import spray.http.HttpHeaders.`If-Modified-Since`
import spray.http.StatusCodes._
import spray.testkit.Specs2RouteTest

class ShippingServiceSpec extends Specification with Specs2RouteTest with ShippingService {
  def actorRefFactory = system

  "ShippingService" should {

    "only addresses added after a timestamp are returned by the rss feed" in {
      Post("/address", AddressFromService("address not in result", "city", Option(""), Option(""), "uuid")) ~> sealRoute(route)
      val cutOffTime = DateTime.now
      Thread.sleep(1001)
      Post("/address", AddressFromService("address in result", "city", Option(""), Option(""), "uuid")) ~> sealRoute(route)
      Get("/rss") ~> addHeader(new `If-Modified-Since`(cutOffTime)) ~> route ~> check {
        val res = responseAs[String]
        res must contain("<street>address in result")
        res must not contain ("<street>address not in result")
      }
    }

    "a UUID is generated if a Address is created without one" in {
      val address = AddressFromService("street 1", "city", Option(""), Option(""), "uuid")
      address.uuid must not be empty
      address.uuid.toString.length must be > 0
    }

    "Post adds a new ShippingAddress" in {
      Post("/address", AddressFromService("Post adds a new ShippingAddress", "city", Option("PostaddsanewShippingAddress"), Option(""), "uuid")) ~> route ~> check {
        status === Created
      }
      Get("/address/PostaddsanewShippingAddress") ~> route ~> check {
        val res = responseAs[String]
        res must contain( """"street": "Post adds a new ShippingAddress"""")
      }
    }

    "Put updates a ShippingAddress" in {
      Post("/address", AddressFromService("Put adds a new ShippingAddress", "city", Option("PutaddsanewShippingAddress"), Option(""), "uuid")) ~> sealRoute(route)
      Get("/address/PutaddsanewShippingAddress") ~> route ~> check {
        val res = responseAs[String]
        res must contain( """"street": "Put adds a new ShippingAddress"""")
      }
      Put("/address", AddressFromService("updated - Put adds a new ShippingAddress", "city", Option("PutaddsanewShippingAddress"), Option(""), "uuid")) ~> route ~> check {
        status === OK
      }
      Get("/address/PutaddsanewShippingAddress") ~> route ~> check {
        val res = responseAs[String]
        res must contain( """"street": "updated - Put adds a new ShippingAddress"""")
      }
    }

    "TransportCost can be added to ShippingData" in {
      Post("/address", AddressFromService("ShippingAddressForTransportCost", "city", Option("uuidShippingAddressForTransportCost"), Option(""), "uuid")) ~> route ~> check {
        status === Created
      }
      Post("/transportCost", TransportCost("uuidShippingAddressForTransportCost", 10.1)) ~> route ~> check {
        status === Created
      }
      Get("/transportCost/uuidShippingAddressForTransportCost") ~> route ~> check {
        responseAs[String] must contain( """"cost": 10.1""")
      }
      Put("/transportCost", TransportCost("uuidShippingAddressForTransportCost", 11.1)) ~> route ~> check {
        status === OK
      }
      Get("/transportCost/uuidShippingAddressForTransportCost") ~> route ~> check {
        responseAs[String] must contain( """"cost": 11.1""")
      }
    }
    // show delivery date (command)
    "Show delivery time returns delivery time as a JSON document" in {
      Post("/orderPlaced", Order("uuidOrderForDeliveryTest", "desc")) ~> route ~> check {
        status === Created
      }
      Get("/deliveryTime/uuidOrderForDeliveryTest") ~> route ~> check {
        responseAs[String] must contain( """"date": 2""")
      }
    }

    "If both transport cost and shipping address are available a ShippingDataComplete message is send" in {
      Post("/address", AddressFromService("ShippingAddressForOutboundMessageTest", "city", Option("uuidShippingAddressForOutboundMessageTest"), Option(""), "uuid")) ~> sealRoute(route)
      Post("/transportCost", TransportCost("ShippingAddressForOutboundMessageTest", 10.1))  ~> route ~> check {
        status === Created
      }
      // Wait for message to appear on queue
    }
    // out: Transport cost available (in rss feed)
    // in: Order available
    // command: Show delivery date
    // command?: Add delivery address   -> implemented through /address put and post methods
    // ShippingService receives a new order
    // Then it waits for shipping options and address data to be added (usually via a user interface)
    // when either becomes available, ShippingService sends out a notification (Kafka?)

    //    "if Shipping receives an address a AddressAvailable event is sent" in {
  }

}
