package kafka.producer

import com.xebia.shipping.service.KafkaProducer

class MockKafkaProducer (topic: String, brokerList: String) extends KafkaProducer(topic, brokerList) {
  //  val producer = new Producer[AnyRef, AnyRef](new ProducerConfig(props))

  override val producer:Producer[AnyRef, AnyRef] = new MockProducer[AnyRef, AnyRef](new ProducerConfig(props)).asInstanceOf[Producer[AnyRef, AnyRef]]
}

class MockProducer[K,V](producerConfig:ProducerConfig) extends Producer[K,V](producerConfig:ProducerConfig) {
  override def send(messages: KeyedMessage[K,V]*): Unit = {
    println("got some messages: " + messages)
  }
}