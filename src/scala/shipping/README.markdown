Shipping allows users to add a shipping address.
------------------------------------------------
When the address is entered (selected from a list, typed in, whatever ...), a AddressAvailable message becomes available
on the RSS feed.
When the transport costs are known, a TransportCostAvailable message is generated (RSS feed)
The delivery date can be queried.
The delivery address can be queried.

Build executable:
-----------------

sbt assembly

This results in a jar file in target/scala-2.11.

Run using:
----------
./sh/run.sh


