package com.example

import akka.actor.Actor
import spray.http.MediaTypes._
import spray.httpx.SprayJsonSupport
import spray.json._
import spray.routing._

import scala.collection.mutable.Map

class MyServiceActor extends Actor with MyService {
  def actorRefFactory = context

  def receive = runRoute(route)
}

case class Person(name: String, firstName: String, age: Int)

object Person extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val personFormat = jsonFormat3(Person.apply)

  implicit def personListFormat[T: JsonFormat] = jsonFormat1(List.apply[T])
}

trait MyService extends HttpService {

  private val personOne = Person("Person1", "First", 1)
  private val personTwo = Person("Person2", "Second", 2)
  private val dummy = Person("Dummy", "Dummy", 5)

  val persons: Map[String, Person] = Map(personOne.name -> personOne, personTwo.name -> personTwo)

  // TODO: this should cause 404
  val route =
      path("person" / Segment) { name =>
        pathEnd {
          respondWithMediaType(`application/json`) {
            val result = persons.get(name)
            result match {
              case Some(_) => complete(result.get.toJson.prettyPrint)
              case _ => complete("")
            }
          }
      }
    } ~
      path("persons") {
        get {
          respondWithMediaType(`application/json`) {
            complete {
              persons.values.toJson.prettyPrint
            }
          }
        }
      } ~
      post {
        path("person") {
          entity(as[Person]) { person =>
            persons += (person.name -> person)
            complete(person.toJson.prettyPrint)
          }
        }
      } ~
      put {
        path("person") {
          entity(as[Person]) { person =>
            persons -= (person.name)
            persons += (person.name -> person)
            complete(person.toJson.prettyPrint)
          }
        }
      }
}
