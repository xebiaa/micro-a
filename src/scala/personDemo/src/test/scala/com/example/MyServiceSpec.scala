package com.example

import org.specs2.mutable.Specification
import spray.testkit.Specs2RouteTest
import spray.http._
import StatusCodes._

class MyServiceSpec extends Specification with Specs2RouteTest with MyService {
  def actorRefFactory = system
  
  "MyService" should {

    "return person1 for GET requests to person/Person1" in {
      Get("/person/Person1") ~> route ~> check {
        responseAs[String] must contain("First")
      }
    }

// TODO: enable this test when we know how to cause 404's
//    "return 404 for GET requests to person/UnknowPerson" in {
//      Get("/person/UnknownPerson") ~> route ~> check {
//        response.status should be equalTo NotFound
//      }
//    }

    "leave GET requests to other paths unhandled" in {
      Get("/kermit") ~> route ~> check {
        handled must beFalse
      }
    }

    "return a new person if one is added using POST" in {
      Post("/person", Person("n1", "f1",47 )) ~> sealRoute(route) ~> check {
        status === OK
        responseAs[String] must contain("f1")
      }
    }

    "return an updated person after a PUT" in {
      Post("/person", Person("n1", "f1",47 )) ~> sealRoute(route) ~> check {
        status === OK
        responseAs[String] must contain("f1")
      }
      Put("/person", Person("n1", "f1-updated",47 )) ~> sealRoute(route) ~> check {
        status === OK
        responseAs[String] must contain("f1-updated")
      }
    }
  }
}
