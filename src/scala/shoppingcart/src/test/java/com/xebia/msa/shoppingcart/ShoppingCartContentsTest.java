package com.xebia.msa.shoppingcart;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Steven Ottenhoff (Dual-IT) on 17/04/15.
 * Copyright 2015
 */
public class ShoppingCartContentsTest {

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testShoppingCartContentsCreate(){

        List<Item> items = new ArrayList<Item>();
        Long id = new Long(1);

        ShoppingCartContents contents = new ShoppingCartContents(id,items);
        Assert.assertEquals(id, contents.getShoppingCartId());
        Assert.assertEquals(items, contents.getItems());
    }
}