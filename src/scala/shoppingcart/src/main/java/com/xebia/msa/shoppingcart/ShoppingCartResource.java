package com.xebia.msa.shoppingcart;

import io.dropwizard.jersey.params.LongParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Steven Ottenhoff (Dual-IT) on 17/04/15.
 * Copyright 2015
 */
@Path("/shoppingcart")
@Produces(MediaType.APPLICATION_JSON)
public class ShoppingCartResource {

    private static final Logger log = LoggerFactory.getLogger(ShoppingCartResource.class);
    private final List<ShoppingCartContents> shoppingCarts = new ArrayList<ShoppingCartContents>();

    public ShoppingCartResource(){
    }

    @GET
    public String getIndex() {
        log.debug("Requested index");

        return "OK";
    }

    @Path("{id}/addProduct")
    @GET
    public String addProductToShoppingCart(@PathParam("id") LongParam id) {

        log.debug("Received request to add product to shopping cart with id " + id);
        return "OK";
    }

    @Path("{id}/orderContents")
    @GET
    public String orderShoppingCartContents(@PathParam("id") LongParam id) {

        log.debug("Received request to order content for shopping cart with id " + id);
        return "OK";
    }

    @Path("{id}/showContents")
    @GET
    public ShoppingCartContents showShoppingCarContents(@PathParam("id") LongParam id) {

        log.debug("Received request to retrieve content for shopping cart with id " + id);
        return new ShoppingCartContents( id.get(), new ArrayList<Item>());
    }



}
