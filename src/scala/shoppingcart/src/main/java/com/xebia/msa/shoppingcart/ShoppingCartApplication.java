package com.xebia.msa.shoppingcart;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

/**
 * Created by Steven Ottenhoff (Dual-IT) on 17/04/15.
 * Copyright 2015
 */
public class ShoppingCartApplication extends Application<ShoppingCartConfiguration> {

    public static void main(String []args) throws Exception {
        new ShoppingCartApplication().run(args);
    }

    @Override
    public void run(ShoppingCartConfiguration shoppingCartConfiguration, Environment environment) throws Exception {

        final ShoppingCartResource resource = new ShoppingCartResource();
        environment.jersey().register(resource);
        final TemplateHealthCheck healthCheck =
                new TemplateHealthCheck();
        environment.healthChecks().register("template", healthCheck);
        environment.jersey().register(resource);
    }
}
