package com.xebia.msa.shoppingcart;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Steven Ottenhoff (Dual-IT) on 17/04/15.
 * Copyright 2015
 */
public class ShoppingCartContents {

    private Long shoppingCartId;
    private List<Item> items;

    public ShoppingCartContents(Long id, List<Item> items){
        this.shoppingCartId = id;
        this.items = items;
    }

    @JsonProperty
    public Long getShoppingCartId() {
        return shoppingCartId;
    }

    @JsonProperty
    public List<Item> getItems() {
        return items;
    }
}
