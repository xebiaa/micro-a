package com.xebia.msa.shoppingcart.event;


import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;

/**
 * Created by Steven Ottenhoff (Dual-IT) on 17/04/15.
 * Copyright 2015
 */
public class KafkaProducer implements Producer<String,String> {

    public Future<RecordMetadata> send(ProducerRecord<String, String> producerRecord) {
        return null;
    }

    public Future<RecordMetadata> send(ProducerRecord<String, String> producerRecord, Callback callback) {
        return null;
    }

    public List<PartitionInfo> partitionsFor(String s) {
        return null;
    }

    public Map<MetricName, ? extends Metric> metrics() {
        return null;
    }

    public void close() {

    }
}
