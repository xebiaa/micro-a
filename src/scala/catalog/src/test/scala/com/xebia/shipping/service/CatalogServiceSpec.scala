package com.xebia.shipping.service

import java.util.Date

import org.specs2.mutable.Specification
import spray.http.DateTime
import spray.http.HttpHeaders.`If-Modified-Since`
import spray.http.MediaTypes._
import spray.http.StatusCodes._
import spray.testkit.Specs2RouteTest

class CatalogServiceSpec extends Specification with Specs2RouteTest with CatalogService {
  def actorRefFactory = system

  "CatalogService" should {

    "a UUID is generated if a Item is created without one" in {
      val item = ItemFromService("name", "description", 1.0, Option(""), Option(""))
      item.uuid must not be empty
      item.uuid.toString.length must be > 0
    }

    "a UUID is generated if a ItemDetail is created without one" in {
      val itemDetails = ItemDetailsFromService("name", 1.0, "green", Option(""))
      itemDetails.uuid must not be empty
      itemDetails.uuid.toString.length must be > 0
    }

    "modifiedAt is filled in if left empty for Item" in {
      val item = ItemFromService("name", "the name", 1.0, Option(""), Option(""))
      item must not be empty
      item.modifiedAt.toString.length must be > 0
    }

    "A uuid is left unchanged if supplied in post or put" in {
      Post("/item", ItemFromService("n1", "f1", 47, Option("non-empty guid"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route) ~> check {
        status === Created
        responseAs[String] must contain("non-empty guid")
        responseAs[String] must not contain ("Some")
      }
      Put("/item", ItemFromService("n1", "f1-updated", 47, Option("non-empty guid"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route) ~> check {
        status === OK
        responseAs[String] must contain("non-empty guid")
      }
    }

    "if a UUID is passed in to an Item, this UUID is used" in {
      val item = ItemFromService("name", "description", 1.0, Option("123"), Option(CatalogService.dateAsString(new Date)))
      item.uuid must not be empty
      item.uuid.toString must beEqualTo(Option("123").toString)
    }

    "return item1 for GET requests to item/Item1" in {
      Get("/item/uid1") ~> route ~> check {
        responseAs[String] must contain("First")
        mediaType must beEqualTo( `application/json`)
      }
    }

    "return rss for GET requests to rss" in {
      Get("/rss") ~> route ~> check {
        val res = responseAs[String]
        res must contain("<rss version=\"2.0\">")
        res must contain("description>Item Item2 has description Second</description>")
        res must contain("<guid>http://" + Boot.host + ":" + Boot.port + "/item/uid1</guid>")
      }
    }

    "return 2 items for GET requests to items" in {
      Get("/items") ~> route ~> check {
        responseAs[String] must contain("First")
        responseAs[String] must contain("Second")
      }
    }

    "return 404 for GET requests to unknown item" in {
      Get("/item/UnknownItem") ~> route ~> check {
        response.status should be equalTo NotFound
      }
    }

    "return 404 for GET requests to unknown itemDetails" in {
      Get("/itemDetails/UnknownItemDetail") ~> route ~> check {
        response.status should be equalTo NotFound
      }
    }

    "leave GET requests to other paths unhandled" in {
      Get("/kermit") ~> route ~> check {
        handled must beFalse
      }
    }

    "return a new item if one is added using POST" in {
      Post("/item", ItemFromService("n1", "f1", 47, Option(null), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route) ~> check {
        status === Created
        responseAs[String] must contain("f1")
        mediaType must beEqualTo( `application/json`)
      }
    }

    "return details for item1 " in {
      Get("/itemDetails/details_uid1") ~> route ~> check {
        responseAs[String] must contain("green")
      }
    }

    "return an updated item after a PUT" in {
      Post("/item", ItemFromService("n1", "f1", 47, Option(""), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route) ~> check {
        status === Created
        responseAs[String] must contain("f1")
      }
      Put("/item", ItemFromService("n1", "f1-updated", 47, Option(""), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route) ~> check {
        status === OK
        responseAs[String] must contain("f1-updated")
        mediaType must beEqualTo( `application/json`)
      }
    }

    "return 2 results for a search for products that look like 'playstation'" in {
      Post("/item", ItemFromService("PlayStation1", "The first Playstation ever", 100, Option("pl1-uuid"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      Post("/item", ItemFromService("PlayStation2", "The second Playstation ever", 200, Option("pl2-uuid"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      Post("/item", ItemFromService("Item3", "Another item that shouldn't be in the result", 200, Option("pl3-uuid"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      Get("/itemLike/playstation") ~> route ~> check {
        status === OK
        val x = responseAs[String]
        responseAs[String] must contain("The first Playstation ever")
        responseAs[String] must contain("The second Playstation ever")
        responseAs[String] must not contain ("Item3")
      }
    }

    "return a list of items modified since a timestamp" in {
      Post("/item", ItemFromService("newItem1", "1", 1, Option("not-modified-since1"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      val cutOffTime = DateTime.now
      Thread.sleep(1001)
      Post("/item", ItemFromService("newItem2", "2", 2, Option("modified-since2"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      Post("/item", ItemFromService("newItem3", "3", 3, Option("modified-since3"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      Get("/rss") ~> addHeader(new `If-Modified-Since`(cutOffTime)) ~> route ~> check {
        val res = responseAs[String]
        res must not contain ("<guid>http://localhost:8081/item/not-modified-since1</guid>")
        res must contain("<guid>http://localhost:8081/item/modified-since2</guid>")
        res must contain("<guid>http://localhost:8081/item/modified-since3</guid>")
      }
    }

    "a post followed by a get of ItemDetails returns the ItemDetails as it was posted" in {
      Post("/item", ItemFromService("itemForDetailsTest", "1", 1, Option("itemForDetailsTest"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      Post("/itemDetails", ItemDetailsFromService("itemForDetailsTest", 1.0, "purple", Option("itemDetailsForItemDetailsTest"))) ~> sealRoute(route)
      Get("/itemDetails/itemDetailsForItemDetailsTest") ~> route ~> check {
        val result = responseAs[String]
        result must contain("itemDetailsForItemDetailsTest")
        result must contain("purple")
        result must contain("itemForDetailsTest")
      }
    }

    "a post for ItemDetails for a Item that doesn't exists results in a 404 error" in {
      Post("/itemDetails", ItemDetailsFromService("NonExistentItem", 1.0, "purple", Option("itemDetailsForItemDetailsTest"))) ~> sealRoute(route) ~> check {
        status === NotFound
        responseAs[String] must contain("Parent 'NonExistentItem' not found")
      }
    }

    "a put with updated ItemDetails should result in updated values" in {
      Post("/item", ItemFromService("ItemSoonToBeUpdated", "1", 1, Option("ItemSoonToBeUpdated"), Option(CatalogService.dateAsString(new Date)))) ~> sealRoute(route)
      Post("/itemDetails", ItemDetailsFromService("ItemSoonToBeUpdated", 1.0, "purple", Option("DetailsSoonToBeUpdated")))  ~> sealRoute(route)
      Put("/itemDetails", ItemDetailsFromService("ItemSoonToBeUpdated", 123.0, "purple2", Option("DetailsSoonToBeUpdated")))  ~> sealRoute(route)
      Get("/itemDetails/DetailsSoonToBeUpdated") ~> sealRoute(route) ~> check {
        status === OK
        responseAs[String] must contain("purple2")
        responseAs[String] must contain("123.0")
      }
    }

    "item and its details can be retrieved using the item's uuid" in {
      Get("/itemPlusDetails/uid2") ~> route ~> check {
        val result = responseAs[String]
        result must contain("details_uid2")
        result must contain("uid2")
        result must contain("red")
        result must contain("20.0")
        result must contain("uid2")
        result must contain("Second")
      }
    }
  }
}
