package com.xebia.shipping.domain

import spray.httpx.SprayJsonSupport
import spray.json.DefaultJsonProtocol

case class Item(name: String, description: String, price: Double, uuid: String, modifiedAt: String)

object Item extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val itemFormat = jsonFormat5(Item.apply)
}

case class ItemDetails(itemUuid: String, weight: Double, color: String, uuid: String)
object ItemDetails extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val itemDetailsFormat = jsonFormat4(ItemDetails.apply)
}

