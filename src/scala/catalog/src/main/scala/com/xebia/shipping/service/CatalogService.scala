package com.xebia.shipping.service

import java.text.{DateFormat, SimpleDateFormat}
import java.util.{Date, UUID}

import akka.actor.Actor
import com.xebia.shipping.domain.{Item, ItemDetails}
import spray.http.HttpRequest
import spray.http.MediaTypes._
import spray.http.StatusCodes._
import spray.httpx.SprayJsonSupport
import spray.json._
import spray.routing._
import spray.routing.directives.{DebuggingDirectives, LoggingMagnet}

import scala.collection.mutable.Map

class CatalogServiceActor extends Actor with CatalogService {
  def actorRefFactory = context

  def receive = runRoute(route)
}

case class ItemFromService(name: String, description: String, price: Double, uuid: Option[String], modifiedAt: Option[String])

case class ItemDetailsFromService(itemUuid: String, weight: Double, color: String, uuid: Option[String])

object ItemDetailsFromService extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val itemDetailsFormat = jsonFormat4(ItemDetailsFromService.apply)

  def toItemDetails(itemDetailsFromService: ItemDetailsFromService) =
    ItemDetails(itemDetailsFromService.itemUuid, itemDetailsFromService.weight, itemDetailsFromService.color, itemDetailsFromService.uuid.getOrElse(UUID.randomUUID.toString))
}

object ItemFromService extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val itemFormat = jsonFormat5(ItemFromService.apply)

  implicit def itemListFormat[T: JsonFormat] = jsonFormat1(List.apply[T])

  def toItem(itemFromService: ItemFromService) = Item(itemFromService.name, itemFromService.description, itemFromService.price, getUuid(itemFromService.uuid), itemFromService.modifiedAt.getOrElse(CatalogService.dateAsString(new Date)))

  def getUuid(uuid:Option[String]):String = {
    if (uuid == None) UUID.randomUUID().toString
    else
    uuid.get match {
      case "" => UUID.randomUUID().toString
      case _ => uuid.get
    }
  }
}

case class ItemPlusDetails(name: String, description: String, price: Double, uuid: String, modifiedAt: String, weight: Double, color: String, detailsUuid: String)

object ItemPlusDetails extends DefaultJsonProtocol with SprayJsonSupport {
  implicit val itemFormat = jsonFormat8(ItemPlusDetails.apply)
}

object CatalogService {
  private val dateFormat: DateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z")

  def dateAsString(date: Date) = dateFormat.format(date)

  def dateFromString(date: String) = dateFormat.parse(date)
}


trait CatalogService extends HttpService {
  private val itemOne = Item("Item1", "First", 1, "uid1", CatalogService.dateAsString(new Date))
  private val itemOneDetails = ItemDetails(itemOne.uuid, 10, "green", "details_uid1")
  private val itemTwo = Item("Item2", "Second", 2, "uid2", CatalogService.dateAsString(new Date))
  private val itemTwoDetails = ItemDetails(itemTwo.uuid, 20, "red", "details_uid2")

  val items: Map[String, Item] = Map(itemOne.uuid -> itemOne, itemTwo.uuid -> itemTwo)
  val itemDetails: Map[String, ItemDetails] = Map(itemOneDetails.uuid -> itemOneDetails, itemTwoDetails.uuid -> itemTwoDetails)

  val itemsDetailsToItems = Map(itemOne.uuid -> itemOneDetails.uuid, itemTwo.uuid -> itemTwoDetails.uuid)

  def getItemsModifiedSince(timeStamp: Long): List[ItemFromService] = List()

  val route =
    path("rss") {
      pathEnd {
        respondWithMediaType(`application/rss+xml`) {
          optionalHeaderValueByName("If-Modified-Since") {
            case Some(modifiedSince) => complete(new CatalogRSSFeed(items, Boot.host + ":" + Boot.port, modifiedSince).feed)
            case _ => complete(new CatalogRSSFeed(items, Boot.host + ":" + Boot.port, Boot.defaultDate).feed)
          }
        }
      }
    } ~
      path("item" / Segment) { uuid =>
        val result = items.get(uuid)
        result match {
          case Some(_) => complete(result.get)
          case _ => complete(NotFound, "Item with key '" + uuid + "' not found")
        }
      } ~
      path("itemDetails" / Segment) { uuid =>
        val result: Option[ItemDetails] = itemDetails.get(uuid)
        result match {
          case Some(_) => complete(result.get)
          case _ => complete(NotFound, "Item details for key '" + uuid + "' not found")
        }
      } ~
      path("itemLike" / Segment) { name =>
        complete(items.filter({ case (k: String, v: Item) => v.name.toUpperCase.contains(name.toUpperCase) }).values)
      } ~
      path("items") {
        complete {
          items.values
        }
      } ~
      post {
        path("item") {
          postItem
        }
      } ~
      post {
        path("itemDetails") {
          postDetails
        }
      } ~
      put {
        path("item") {
          updateItem
        }
      } ~
      put {
        path("itemDetails") {
          updateDetails
        }
      } ~
      path("itemPlusDetails" / Segment) { uuid =>
        val item = items.get(uuid)
        item match {
          case Some(_) => {
            val theItem = item.get
            val detailsUuid = itemsDetailsToItems.get(uuid)
            val details: Option[ItemDetails] = itemDetails.get(detailsUuid.get)
            val theDetails = details.get
            val itemPlusDetails = ItemPlusDetails(theItem.name, theItem.description, theItem.price, theItem.uuid, theItem.modifiedAt
              , theDetails.weight, theDetails.color, theDetails.uuid)
            complete(itemPlusDetails)
          }
          case _ => complete(NotFound, "Item with key '" + uuid + "' not found")
        }
      }

  def postItem:Route = postOrUpdateItem("post")
  def updateItem:Route = postOrUpdateItem("put")
  def postOrUpdateItem(method:String): Route = respondWithMediaType(`application/json`) {
    entity(as[ItemFromService]) { item =>
      val newItem = ItemFromService.toItem(item)
      items += (newItem.uuid -> newItem)
      method match {
        case "post" => complete(Created, newItem)
        case "put" => complete(OK, newItem)
      }
    }
  }

  def postDetails:Route = postOrUpdateDetails("post")
  def updateDetails:Route = postOrUpdateDetails("update")
  def postOrUpdateDetails(method:String): Route = respondWithMediaType(`application/json`) {
    entity(as[ItemDetailsFromService]) { details =>
      val newItemDetails = ItemDetailsFromService.toItemDetails(details)
      val parent = items.get(details.itemUuid)
      parent match {
        case None => complete(NotFound, "Parent '" + details.itemUuid + "' not found")
        case _ => {
          itemDetails += (newItemDetails.uuid -> newItemDetails)
          method match {
            case "post" => complete(Created, newItemDetails)
            case "update" => complete(OK, newItemDetails)
          }
        }
      }
    }
  }

  def printResponseStatus(res: Any): Unit = println(res)

  val logResponsePrintln = DebuggingDirectives.logResponse(LoggingMagnet(printResponseStatus))

  def printRequest(req: HttpRequest): Unit = {
    println(req)
  }

  val logRequestPrintln = DebuggingDirectives.logRequest(LoggingMagnet(printRequest))
}
