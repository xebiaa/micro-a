package com.xebia.shipping.service

import java.util.Date

import com.xebia.shipping.domain.Item

import scala.collection.mutable.Map

class CatalogRSSFeed(items: Map[String, Item], baseUri: String, modifiedSinceHeader:String) {
  private val modifiedSince = CatalogService.dateFromString(modifiedSinceHeader)
  private val now = CatalogService.dateAsString(new Date)
  val feed =
    <rss version="2.0">
      <channel>
        <title>Item</title>
        <description>Item shows basic data for items in the catalog</description>
        <link>{baseUri}/rss</link>
        <lastBuildDate>{now}</lastBuildDate>
        <pubDate>{now}</pubDate>
        
        {for (item <- items.values if (CatalogService.dateFromString(item.modifiedAt).after(modifiedSince))) yield {
        <item>
          <title>
            {item.name}
          </title>
          <description>Item {item.name} has description {item.description}</description>
          <link>http://{baseUri}/item/{item.uuid}</link>
          <guid>http://{baseUri}/item/{item.uuid}</guid>
          <pubDate>{now}</pubDate>
        </item>
      }}
        
      </channel>
    </rss>
}
