package com.xebia.junk;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.util.Assert;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Arrays;

import static junit.framework.TestCase.fail;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class ScenarioTest {
    protected HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {

        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream().filter(
                hmc -> hmc instanceof MappingJackson2HttpMessageConverter).findAny().get();

        org.junit.Assert.assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, MediaType.APPLICATION_JSON, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }


    //@Test
    public void testFulfillmentAcceptsOrders() {
        try {
                                            //curl -X POST -d "fizz=buzz" http://requestb.in/18bafx21
            String orderFromCart = "{\"uuid\":\"d45b44e1-4f66-4803-8f1d-9e0b9f2e9651\",\"ordered\":1438319100894,\"shipped\":null,\"shippingAddress\":\"address1\",\"status\":\"Ordered\",\"total\":0.0,\"shoppingCart\":{\"created\":1438319100894,\"lineItems\":[{\"uuid\":\"96d055b0-cb34-49a9-b165-35f5c48b4ccd\",\"quantity\":2,\"product\":{\"uuid\":\"755b9908-0b28-484a-a9cc-4aa44bc5a681\",\"name\":\"product1\",\"supplier\":\"supplier1\",\"price\":10.0},\"price\":10.0}],\"uuid\":\"d415e1c8-ccf6-42ca-9e1d-ff2169860ba8\",\"total\":0.0},\"account\":{\"uuid\":\"15fbad5c-b65c-4d28-b318-49ea334400eb\",\"address\":\"address1\",\"phoneNumber\":\"phoneNumber1\",\"email\":\"email1\"},\"paymentReceived\":false,\"links\":[{\"rel\":\"self\",\"href\":\"http://localhost/cart/orders/c45b44e1-4f66-4803-8f1d-9e0b9f2e9651\"}]}";
//            Content result = Request.Post("http://localhost:9003/fulfillment/orders/newOrder")
            Content result = Request.Post("http://requestb.in/18bafx21/fulfillment/orders/newOrder")
                    .bodyString(orderFromCart, ContentType.APPLICATION_JSON).execute().returnContent();
            String data = result.asString();
            assert(data.indexOf("\"quantity\":1")>0);
        } catch (IOException e) {
            fail("Exception " + e.getMessage());
        }
    }

}
